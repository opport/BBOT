#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);
    ui->aboutCopyrightLabel->setText(QCoreApplication::organizationName() + " © 2015-2016");
    ui->aboutProductNameLabel->setText(QApplication::applicationName() + " v" + QApplication::applicationVersion());
}

About::~About()
{
    delete ui;
}
