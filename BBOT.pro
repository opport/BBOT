#-------------------------------------------------
#
# Project created by QtCreator 2015-07-27T17:13:55
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BBOT
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    about.cpp \
    exit.cpp \
    miniwindow.cpp \
    mediumwindow.cpp

HEADERS  += mainwindow.h \
    about.h \
    exit.h \
    miniwindow.h \
    mediumwindow.h

FORMS    += mainwindow.ui \
    about.ui \
    exit.ui \
    miniwindow.ui \
    mediumwindow.ui

DISTFILES += \
    securityAPI/SilkroadSecurityApi.tlb \
    securityAPI/SilkroadSecurityApi.pdb \
    securityAPI/SilkroadSecurityApi.dll

RESOURCES += \
    resource.qrc
