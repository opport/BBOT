#include "mediumwindow.h"
#include "ui_mediumwindow.h"

MediumWindow::MediumWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MediumWindow)
{
    ui->setupUi(this);

    //Lock size
    this->setFixedSize(this->geometry().width(),this->geometry().height());

    //Make frameless
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Window );

    // Set progress bar colors
    QPalette palette = ui->beserkProgressBar->palette();

    palette.setColor(QPalette::Highlight,Qt::red);
    ui->HPProgressBar->setPalette(palette);
    palette.setColor(QPalette::Highlight,Qt::magenta);
    ui->beserkProgressBar->setPalette(palette);
    palette.setColor(QPalette::Highlight,Qt::darkYellow);
    ui->expProgressBar->setPalette(palette);


    // Initialize window
    mini = new MiniWindow(this);
}

MediumWindow::~MediumWindow()
{
    delete ui;
}

void MediumWindow::mousePressEvent(QMouseEvent *event){
    this->mouse_position = event->pos();
}

void MediumWindow::mouseMoveEvent(QMouseEvent *event){
    if (event->buttons() & Qt::LeftButton) {
        QPoint diff = event->pos() - this->mouse_position;
        QPoint newpos = this->pos() + diff;

        this->move(newpos);
    }
}

void MediumWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, qAppName(),
                                                                tr("Are you sure?\n"),
                                                                QMessageBox::Yes | QMessageBox::Cancel,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
    }
}

void MediumWindow::on_actionExit_triggered()
{
    this->close();
}

void MediumWindow::on_actionMinimize_triggered()
{
    this->setWindowState(Qt::WindowMinimized);
}

void MediumWindow::on_actionMainWindow_triggered()
{
    this->parentWidget()->show();
    this->hide();

}

void MediumWindow::on_actionMinimode_triggered()
{
    mini->show();
    this->hide();
}
