#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QtGui>
#include <QMainWindow>
#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QRegExpValidator>
#include "about.h"
#include "mediumwindow.h"
#include "miniwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void closeEvent(QCloseEvent *event);
    void on_Botoptions_destroyed(QObject *arg1);

    void on_aboutBBOT_triggered();

    void on_actionMinimode_triggered();

    void on_actionMediummode_triggered();

    void on_actionMinimize_Bot_triggered();

    void on_actionExit_triggered();

    void on_clientFolderButton_clicked();

    void on_townScriptToolButton_clicked();

    void on_trainingScriptToolButton_clicked();

private:
    Ui::MainWindow *ui;
    About *about;
    MediumWindow *medium;

    void set_IPValidator();
};

#endif // MAINWINDOW_H
