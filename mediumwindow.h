#ifndef MEDIUMWINDOW_H
#define MEDIUMWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QMessageBox>
#include "miniwindow.h"

namespace Ui {
class MediumWindow;
}

class MediumWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MediumWindow(QWidget *parent = 0);
    ~MediumWindow();
    MiniWindow *mini;

private slots:
    void closeEvent(QCloseEvent *event);
    void on_actionExit_triggered();

    void on_actionMinimize_triggered();

    void on_actionMainWindow_triggered();

    void on_actionMinimode_triggered();

private:
    Ui::MediumWindow *ui;
    QPoint mouse_position;
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
};

#endif // MEDIUMWINDOW_H
