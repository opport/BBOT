#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Lock size
    this->setFixedSize(this->geometry().width(),this->geometry().height());
    // Set attributes
    QCoreApplication::setOrganizationName("PR");
    QCoreApplication::setOrganizationDomain("https://gitlab.com/opport/BBOT");
    QCoreApplication::setApplicationName("BBOT");
    QCoreApplication::setApplicationVersion("0.1");
    QCoreApplication::organizationName();

    //Set input masks
    //Login tab
    set_IPValidator();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, qAppName(),
                                                                tr("Are you sure?\n"),
                                                                QMessageBox::Yes | QMessageBox::Cancel,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
    }
}

void MainWindow::set_IPValidator()
{
    QRegExpValidator *validator = new QRegExpValidator(this);
    QRegExp rx("((1{0,1}[0-9]{0,2}|2[0-4]{1,1}[0-9]{1,1}|25[0-5]{1,1})\\.){3,3}(1{0,1}[0-9]{0,2}|2[0-4]{1,1}[0-9]{1,1}|25[0-5]{1,1})");
    validator->setRegExp(rx);
    ui->serverAddressLineEdit->setValidator(validator);
    //ui->serverAddressLineEdit->setInputMask("000.000.000.000");
}

void MainWindow::on_Botoptions_destroyed(QObject *arg1)
{
    Q_UNUSED(arg1);
}

void MainWindow::on_aboutBBOT_triggered()
{
    about = new About(this);
    about->show();
}

void MainWindow::on_actionMinimode_triggered()
{
    medium = new MediumWindow(this);
    medium->mini->show();
    this->hide();
}

void MainWindow::on_actionMediummode_triggered()
{
    medium = new MediumWindow(this);
    medium->show();
    this->hide();
}

void MainWindow::on_actionMinimize_Bot_triggered()
{
    this->setWindowState(Qt::WindowMinimized);
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::on_clientFolderButton_clicked()
{
    //QFileDialog *dialog = new QFileDialog(this);

    QString directory = QFileDialog::getExistingDirectory(this,tr("SRO_Client.exe directory"),QDir::homePath(),
                                                     QFileDialog::ShowDirsOnly |
                                                     QFileDialog::DontUseNativeDialog |
                                                     QFileDialog::ReadOnly);
    ui->clientFolderLineEdit->setText(directory);
}

// Plain text ([^.]+);;     //for linux :'(
void MainWindow::on_townScriptToolButton_clicked()
{
    QString file = QFileDialog::getSaveFileName(this,tr("SRO_Client.exe directory"),QDir::homePath(),
                                                tr("Text files (*.txt *.ini);; All files (*.*)"),0, QFileDialog::DontUseNativeDialog);
    ui->townScriptLineEdit->setText(file);
}

void MainWindow::on_trainingScriptToolButton_clicked()
{
    QString file = QFileDialog::getSaveFileName(this,tr("SRO_Client.exe directory"),QDir::homePath(),
                                                tr("Text files (*.txt *.ini);; All files (*.*)"),0, QFileDialog::DontUseNativeDialog);
    ui->trainingScriptLineEdit->setText(file);
}
