#ifndef MINIWINDOW_H
#define MINIWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>
#include <QMessageBox>
#include <QShortcut>

namespace Ui {
class MiniWindow;
}

class MiniWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MiniWindow(QWidget *parent = 0);
    ~MiniWindow();

private slots:
    void closeEvent(QCloseEvent *event);
    void on_actionExit_triggered();

    void on_actionMediummode_triggered();

    void on_actionMainWindow_triggered();
    
    void on_actionMinimize_triggered();

private:
    Ui::MiniWindow *ui;
    QPoint mouse_position;
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    QShortcut *keyMinimize = new QShortcut(QKeySequence(Qt::META + Qt::Key_M), this, SLOT(on_actionMinimize_triggered()));
    QShortcut *keyMediumMode = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_2), this, SLOT(on_actionMediummode_triggered()));
    QShortcut *keyMainWindow = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_3), this, SLOT(on_actionMainWindow_triggered()));
};

#endif // MINIWINDOW_H
