#include "miniwindow.h"
#include "ui_miniwindow.h"

MiniWindow::MiniWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MiniWindow)
{
    ui->setupUi(this);

    //Lock size
    this->setFixedSize(this->geometry().width(),this->geometry().height());

    //Make frameless
    this->setWindowFlags(Qt::FramelessWindowHint | Qt::Window );

    //Set progress bar colors
    QPalette palette = ui->beserkProgressBar->palette();

    palette.setColor(QPalette::Highlight,Qt::red);
    ui->HPProgressBar->setPalette(palette);
    palette.setColor(QPalette::Highlight,Qt::magenta);
    ui->beserkProgressBar->setPalette(palette);
    palette.setColor(QPalette::Highlight,Qt::darkYellow);
    ui->expProgressBar->setPalette(palette);
}

MiniWindow::~MiniWindow()
{
    delete ui;
}

void MiniWindow::mousePressEvent(QMouseEvent *event){
    this->mouse_position = event->pos();
}

void MiniWindow::mouseMoveEvent(QMouseEvent *event){
    if (event->buttons() & Qt::LeftButton) {
        QPoint diff = event->pos() - this->mouse_position;
        QPoint newpos = this->pos() + diff;

        this->move(newpos);
    }
}

void MiniWindow::closeEvent(QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, qAppName(),
                                                                tr("Are you sure?\n"),
                                                                QMessageBox::Yes | QMessageBox::Cancel,
                                                                QMessageBox::Yes);
    if (resBtn != QMessageBox::Yes) {
        event->ignore();
    } else {
        event->accept();
    }
}

void MiniWindow::on_actionExit_triggered()
{
    this->close();
}

void MiniWindow::on_actionMainWindow_triggered()
{
    parentWidget()->parentWidget()->show();
    this->hide();
}

void MiniWindow::on_actionMediummode_triggered()
{
    parentWidget()->show();
    this->hide();
}

void MiniWindow::on_actionMinimize_triggered()
{
    this->setWindowState(Qt::WindowMinimized);
}
